const onionUrlMap = {
  'gsxohj375bk7gjal.onion': 'http://rjrsgw3y2wzqmnvv.onion',
  'y7pm6of53hzeb7u2.onion': 'http://qbnprwaqyglijwqq.onion',
}
export function determineBaseUrl(donateProccessorFallbackUrl) {
  const host = window.location.host;
  const onionBaseUrl = onionUrlMap[host];
  if (onionBaseUrl == undefined) {
    return donateProccessorFallbackUrl;
  } else {
    return onionBaseUrl;
  }
}
