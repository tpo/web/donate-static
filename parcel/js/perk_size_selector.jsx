import React from 'react';

import {ShirtSizeSelector} from './shirt_size_selector';
import {SweatshirtSizeSelector} from './sweatshirt_size_selector';

export function PerkSizeSelector(props) {
  const {selectedPerk, perkOption, perkOptionProperties, fitsAndSizes, updateFitsAndSizes, shirtFits, sweatshirtSizes} = props;

  const sizeOptions = (shirt) => {
    if (fitsAndSizes[shirt + "Fit"]) {
      let option = 'tor-onions';
      if (shirt == 'shirt2') {
        option = 'powered-by-privacy';
      }
      const fit = fitsAndSizes[shirt + "Fit"];

      if (shirtFits[option]['fits'][fit]) {
        return (Object.keys(shirtFits[option]['fits'][fit]['sizes']).map(id =>
          <option value={shirtFits[option]['fits'][fit]['sizes'][id]} key={shirtFits[option]['fits'][fit]['sizes'][id]}>
            {shirtFits[option]['fits'][fit]['sizes'][id].toUpperCase()}
          </option>
        ));
      }
    }
    return null;
  }

  const getPerkFields = () => {
    if (selectedPerk == 't-shirt') {
      return (
        <ShirtSizeSelector
          shirt='shirt1'
          shirtType='tor-onions'
          perkOption={perkOption}
          perkOptionProperties={perkOptionProperties}
          shirtFits={shirtFits}
          sizeOptions={sizeOptions('shirt1')}
          fitsAndSizes={fitsAndSizes}
          updateFitsAndSizes={updateFitsAndSizes}
        />
      );
    } else if (selectedPerk == 't-shirt-pack') {
      return (
        <React.Fragment>
          <ShirtSizeSelector
            shirt='shirt1'
            perkOption={perkOption}
            perkOptionProperties={perkOptionProperties}
            shirtFits={shirtFits}
            sizeOptions={sizeOptions('shirt1')}
            fitsAndSizes={fitsAndSizes}
            updateFitsAndSizes={updateFitsAndSizes}
          />
          <ShirtSizeSelector
            shirt='shirt2'
            perkOption={perkOption}
            perkOptionProperties={perkOptionProperties}
            shirtFits={shirtFits}
            sizeOptions={sizeOptions('shirt2')}
            fitsAndSizes={fitsAndSizes}
            updateFitsAndSizes={updateFitsAndSizes}
          />
        </React.Fragment>
      );
    } else if (selectedPerk == 'sweatshirt') {
      return (
        <SweatshirtSizeSelector
          sweatshirtSizes={sweatshirtSizes}
          updateFitsAndSizes={updateFitsAndSizes}
          fitsAndSizes={fitsAndSizes}
        />
      );
    }
    return null;
  }

  if (selectedPerk == 'stickers' || !selectedPerk) {
    return null;
  } else {
    return (
      <React.Fragment>
        <div id="perk-fields">
          <div className="perk-fields-instructions strong">
            Choose your size and fit.
          </div>
          {getPerkFields()}
        </div>
      </React.Fragment>
    );
  }
}
