import React from 'react';
import {useState} from 'react';

export function LoadingDialogReactPages(props) {
  const {open} = props;
  if (open) {
    return (
      <div id="loading-dialog">
        <div className="overlay">
          <div className="dialog-area">
            <div className="dialog">
              <div className="dots">
                <div className="dot"></div>
                <div className="focus dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
                <div className="dot"></div>
              </div>
              <h5 className="message">
                One moment while we shovel coal into our servers.
              </h5>
            </div>
          </div>
        </div>
      </div>
    );
  } else {
    return null;
  }
}
