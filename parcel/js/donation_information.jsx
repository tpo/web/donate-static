import React, {useState} from 'react';

import {showCommaForThousands} from './number';

export function DonationInformation(props) {
  const {selectedPrice, frequency} = props;

  let label = ' $' + showCommaForThousands(selectedPrice / 100);
  if (frequency == 'monthly') {
    label += ' per month';
  }

  return (
    <span id="donate-submit-amount">{label}</span>
  );
}
