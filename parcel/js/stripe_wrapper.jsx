import React from 'react';
import {StripeProcessor} from './stripe_processor';

import {loadStripe} from '@stripe/stripe-js';
import {Elements} from '@stripe/react-stripe-js';


export function StripeWrapper(props) {
  const {stripePublishableKey, setErrors, formState, fieldsData, perkData, tokenData, frequency, donateProccessorBaseUrl, setLoading, successRedirectUrl, paymentMethod, onStripeFieldChange, errors, selectedPrice} = props;

  const stripePromise = loadStripe(stripePublishableKey);

  return (
    <Elements stripe={stripePromise}>
      <StripeProcessor formState={formState} setErrors={setErrors} fieldsData={fieldsData} perkData={perkData} tokenData={tokenData} frequency={frequency} donateProccessorBaseUrl={donateProccessorBaseUrl} setLoading={setLoading} successRedirectUrl={successRedirectUrl} paymentMethod={paymentMethod} errors={errors} onStripeFieldChange={onStripeFieldChange} selectedPrice={selectedPrice} />
    </Elements>
  );
}
