import "core-js/stable";
import {GivingForm} from './giving_form';
import {CampaignTotals} from './campaign_totals';
import React from 'react';
import ReactDOM from 'react-dom';
import {AppContext, initializeAppContext} from './app_context';

const reactCallbacks = {};
const availableComponents = {
  'GivingForm': GivingForm,
  'CampaignTotals': CampaignTotals,
};

if ('reactComponents' in window) {
  for (const reactComponent of window.reactComponents) {
    const element = document.getElementById(reactComponent.id);
    const props = reactComponent.props;
    const appContext = initializeAppContext(props);
    if (element !== null) {
      const ComponentToUse = availableComponents[reactComponent.name];
      const markup = (
        <AppContext.Provider value={appContext}>
          <ComponentToUse callbacks={reactCallbacks} {...reactComponent.props} />
        </AppContext.Provider>
      );
      ReactDOM.render(markup, element);
    }
  }
}

window.tor = {
  reactCallbacks
};
