import React from 'react';
import {buildUrl} from './assets';

export function getPaymentMethods(appContext) {
  const payPalImageUrl = buildUrl(appContext, 'static/images/donate/PayPal.svg.png');
  return [
    {
      'name': 'credit-card',
      'label': 'Credit Card'
    },
    {
      'name': 'paypal',
      'label': (<img name="paypal" className="paypal paypal-png" src={payPalImageUrl} />),
    }
  ];
};

export const sweatshirtSizes = ['s', 'm', 'l', 'xl', 'xxl', '3xl', '4xl'];

export function requiredFields(paymentMethod, perk) {
  if (paymentMethod == 'credit-card') {
    return ['firstName', 'lastName', 'email', 'streetAddress', 'country', 'locality', 'region', 'postalCode', 'captcha'];
  } else if (perk) {
    return ['firstName', 'lastName', 'email', 'streetAddress', 'country', 'locality', 'region', 'postalCode', 'captcha'];
  } else {
    return ['firstName', 'lastName', 'email', 'captcha'];
  }
};

export const textFields = {
  'firstName': {
    'placeholder': 'First Name',
    'maxLength': 256,
    'type': 'text',
  },
  'lastName': {
    'placeholder': 'Last Name',
    'maxLength': 256,
    'type': 'text',
  },
  'streetAddress': {
    'placeholder': 'Street Address',
    'maxLength': 256,
    'type': 'text',
  },
  'extendedAddress': {
    'placeholder': 'Apt.',
    'maxLength': 256,
    'type': 'text',
  },
  'locality': {
    'placeholder': 'City',
    'maxLength': 256,
    'type': 'text',
  },
  'postalCode': {
    'placeholder': 'Postal Code',
    'maxLength': 256,
    'type': 'text',
  },
  'email': {
    'placeholder': 'Email Address',
    'maxLength': 256,
    'type': 'text',
  },
  'captcha': {
    'placeholder': 'captcha',
    'type': 'text',
    'maxLength': 5
  },
};

export const stripeTokenFieldMap = {
  'country': 'address_country',
  'email': 'name',
  'extendedAddress': 'address_line2',
  'postalCode': 'address_zip',
  'region': 'address_state',
  'streetAddress': 'address_line1',
  'locality': 'address_city',
};

export const displayPerkSelections = {
  'single': true,
  'monthly': true,
  'both': true,
};

export const restrictedCountries = [
  'RU',
  'UA',
];
