import React from 'react';

export function PerkImage(props) {
  const {perk, perkOption, frequency} = props;

  let imageSource = perk.image[frequency];

  if (perk.options !== null) {
    for (const option of perk.options) {
      if (option.name == perkOption) {
        imageSource = option.image[frequency];
      }
    }
  }

  return (
    <img name={perk.name} src={imageSource} />
  );
}
