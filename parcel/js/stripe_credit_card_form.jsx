import React, {useContext} from 'react';
import {NamedError, findErrorByName} from './named_error';
import {CardNumberElement, CardExpiryElement, CardCvcElement} from '@stripe/react-stripe-js';
import {useElements, useStripe} from '@stripe/react-stripe-js';
import {AppContext} from './app_context';
import {buildUrl} from './assets';

const createOptions = (placeholder, fieldName, errors) => {
  let classes = ['field', fieldName];

  const fieldMapping = {
    'card-number': 'cardNumber',
    'exp-date': 'cardExpiry',
    'cvc': 'cardCvc'
  };

  if (findErrorByName(errors, fieldMapping[fieldName]) != undefined) {
    classes.push('error');
  }

  return {
    placeholder: placeholder,
    classes: {
      base: classes.join(" "),
    },
    style: {
      base: {
        fontSize: '16px',
        color: '#484848',
        letterSpacing: '0.025em',
        '::placeholder': {
          fontFamily: '"Source Sans Pro", sans-serif'
        },
        fontFamily: '"Source Sans Pro", sans-serif'
      },
      invalid: {
        color: '#484848',
      },
    },
  };
};

export function StripeCreditCardForm(props) {
  const {onStripeFieldChange, errors, stripe, elements} = props;
  const appContext = useContext(AppContext);
  const creditCardImageUrl = buildUrl(appContext, 'static/images/donate/credit-cards.png');

  if (!stripe || !elements) { return null; }

  return (
    <React.Fragment>
      <div className="split-form stripe-elements">
        <div className="field-row">
          <CardNumberElement
            options={createOptions('Card Number', 'card-number', errors)}
            onChange={onStripeFieldChange}
          />
        <img className='credit-cards' src={creditCardImageUrl} />
        </div>
        <div className="field-row">
          <CardExpiryElement
            options={createOptions('MM/YY', 'exp-date', errors)}
            onChange={onStripeFieldChange}
          />
          <CardCvcElement
            options={createOptions('CVC', 'cvc', errors)}
            onChange={onStripeFieldChange}
          />
        </div>
      </div>
    </React.Fragment>
  );
}
