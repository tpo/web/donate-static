import React, {useState} from 'react';

export function DonateButton(props) {
  const {paymentMethod, stripeSubmitHandle} = props;

  if (paymentMethod == 'credit-card') {
    return (
      <div id="donate-submit-button">
        <button onClick={stripeSubmitHandle} className="donate button" type="submit" value="Donate">Donate</button>
      </div>
    );
  }
  return null;
}
