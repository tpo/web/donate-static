import React, {useState} from 'react';

import {Checkbox} from './checkbox';
import {CountryDropdown} from './country_dropdown';
import {findErrorByName} from './named_error';
import {RegionDropdown} from './region_dropdown';
import {PerkSizeSelector} from './perk_size_selector';
import countries from './countries';
import regions from './regions';
import {DonateButton} from './donate_button';
import {PayPalButton} from './pay_pal_button';
import {StripeWrapper} from './stripe_wrapper';
import {DonationInformation} from './donation_information';

export function GivingInfoForm(props) {
  const {paymentMethod, mailingListOptIn, onMailingListOptInCheckboxChange, selectedPerk, perkOption, perkOptionProperties, shirtFits, sweatshirtSizes, frequency, selectedPrice, noPerk, selectedPerkFriendlyName, requiredFields, errors, textFields, priceOtherRef, formData, countryChanged, regionChanged, onInputFieldChange, fitsAndSizes, updateFitsAndSizes, stripeSubmitHandle, stripe, isValidEmail, validateRequiredFieldsAndDonationAmount, preparePerkData, prepareFieldsData, createBillingAgreement, onStripeFieldChange, addError, displayPerkSelection, donateProccessorBaseUrl, successRedirectUrl, setLoading, stripePublishableKey, createStripeTokenData, paymentMethods, formState, setErrors, restrictedCountries} = props;

  const donationInformation = () => {
    if (displayPerkSelection) {
      if (noPerk) {
        return (<span id="donate-submit-perk">No Gift Selected</span>);
      } else {
        return (<span id="donate-submit-perk">Gift selected: {selectedPerkFriendlyName}</span>);
      }
    } else {
      return;
    }
  }

  const getInputTextField = (name) => {
    let classes = ['field'];

    const error = findErrorByName(errors, name);
    if (error != undefined) {
      classes.push('error');
    } else if (requiredFields.includes(name)) {
      classes.push('required');
    }

    return (
      <input
        className={classes.join(' ')}
        name={name}
        aria-label={name}
        id={name}
        maxLength={textFields[name]['maxLength']}
        placeholder={textFields[name]['placeholder']}
        type={textFields[name]['type']}
        onChange={onInputFieldChange}
      />
    );
  };

  const getStripe = () => {
    if (paymentMethods.includes('credit-card')) {
      const fieldsData = prepareFieldsData();
      const perkData= preparePerkData();
      const tokenData = createStripeTokenData();

      return (
        <StripeWrapper formState={formState} setErrors={setErrors} fieldsData={fieldsData} perkData={perkData} stripePublishableKey={stripePublishableKey} tokenData={tokenData} frequency={frequency} donateProccessorBaseUrl={donateProccessorBaseUrl} setLoading={setLoading} successRedirectUrl={successRedirectUrl} paymentMethod={paymentMethod} onStripeFieldChange={onStripeFieldChange} errors={errors} selectedPrice={selectedPrice} />
      );
    }
    return null;
  }

  const noPerkCheckbox = noPerk ? 'on' : 'off';

  return(
    <React.Fragment>
      <h4 className="info-txt">Your Info</h4>
      <div className="required">* required fields</div>
      <div className="field-row">
        {getInputTextField('firstName')}
        {getInputTextField('lastName')}
      </div>
      <div className="field-row">
        {getInputTextField('streetAddress')}
        {getInputTextField('extendedAddress')}
      </div>
      <div className="field-row flex-column">
        <CountryDropdown
          countries={countries}
          countryChanged={countryChanged}
          selectedCountry={formData['country']}
          required={requiredFields.includes('country')}
          restrictedCountries={restrictedCountries}
          errors={errors}
          setErrors={setErrors}
        />
      </div>
      <div className="field-row">
        {getInputTextField('locality')}
        <RegionDropdown regions={regions}
          selectedCountry={formData['country']}
          onChange={regionChanged}
          required={requiredFields.includes('region')}
          errors={errors}
        />
        {getInputTextField('postalCode')}
      </div>
      <div className="field-row">
        {getInputTextField('email')}
        <div className="receipt light">We&lsquo;ll email you your receipt</div>
      </div>
      <div className="field-row">
        <Checkbox name="mailingListOptInCheckbox" checked={mailingListOptIn} onChange={onMailingListOptInCheckboxChange}/>
        <label htmlFor="mailingListOptInCheckbox">Start sending me email updates about the Tor Project!</label>
      </div>
      {getStripe()}
      <PerkSizeSelector
        selectedPerk={selectedPerk}
        perkOption={perkOption}
        perkOptionProperties={perkOptionProperties}
        fitsAndSizes={fitsAndSizes}
        updateFitsAndSizes={updateFitsAndSizes}
        shirtFits={shirtFits}
        sweatshirtSizes={sweatshirtSizes}
      />
      <div id="donate-comments-wrapper">
        <div className="strong">Comments</div>
        <textarea id="donate-comments" name="comments" aria-label="Comments" placeholder="Comments" onChange={onInputFieldChange} ></textarea>
      </div>
      <div className="donate-submit-area">
        Donating:
        <DonationInformation
          selectedPrice={selectedPrice}
          frequency={frequency}
        />
        <br />
        {donationInformation()}
      </div>
      <div className="captcha">
        Enter the following 4 letters (case insensitive) <img src={donateProccessorBaseUrl + "/captcha"} border="0" />
      {getInputTextField('captcha')}
      </div>
      <DonateButton
        paymentMethod={paymentMethod}
        stripeSubmitHandle={stripeSubmitHandle}
      />
      <PayPalButton
        paymentMethod={paymentMethod}
        amount={selectedPrice}
        perk={selectedPerk}
        frequency={frequency}
        formData={formData}
        noPerkCheckbox={noPerkCheckbox}
        fitsAndSizes={fitsAndSizes}
        perkOption={perkOption}
        requiredFields={requiredFields}
        textFields={textFields}
        isValidEmail={isValidEmail}
        validateRequiredFieldsAndDonationAmount={validateRequiredFieldsAndDonationAmount}
        preparePerkData={preparePerkData}
        prepareFieldsData={prepareFieldsData}
        createBillingAgreement={createBillingAgreement}
        addError={addError}
        donateProccessorBaseUrl={donateProccessorBaseUrl}
        successRedirectUrl={successRedirectUrl}
        setLoading={setLoading}
      />
    </React.Fragment>
  );
}
